Для запуска теста необходимо, чтобы была установлена Java версии 8+, задана переменная окружения JAVA_HOME и в PATH добавлен путь до бинарных файлов SDK.

Тест запускается командой в консоли:
./gradlew -Dyandex.username=your_username@yandex.ru -Dyandex.password=your_password -Dselenide.browser=choose_browser

Значения по умолчанию для параметров:
yandex.username=footestbar@yandex.ru
yandex.password=password1
selenide.browser=firefox