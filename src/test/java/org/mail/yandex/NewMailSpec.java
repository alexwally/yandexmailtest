package org.mail.yandex;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import java.util.UUID;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static org.mail.yandex.Highlighter.highlight;

public class NewMailSpec extends YandexMailTest {
    private String subj = UUID.randomUUID().toString();

    @Test
    public void userCanComposeEmail() {
        //отправляем письмо самому себе
        $("span.b-toolbar__col:nth-child(1) > a:nth-child(1)").click();
        $(By.name("to")).setValue(username).pressTab();
        $(By.name("subj")).setValue(subj).pressTab();
        $(By.name("send")).setValue("Deon Vog");
        $(By.name("doit")).click();

        //проверяем, что письмо появилось в "отправленных"
        $(byAttribute("href", "/lite/sent")).click();
        highlight($(byText(subj)).shouldBe(visible));

        //чистим папку "отправленные"
        clearInputMail();
    }

    private void clearInputMail() {
        $("#check-all").click();
        $("span.b-toolbar__col:nth-child(4) > input:nth-child(1)").click();
    }
}
