package org.mail.yandex;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.junit.ScreenShooter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;

import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;
import static com.codeborne.selenide.WebDriverRunner.addListener;

public class YandexMailTest {
    protected static String username = System.getProperty("yandex.username");
    private static String password = System.getProperty("yandex.password");
    private static String selenideBrowser = System.getProperty("selenide.browser");

    @BeforeAll
    public static void openInbox() {
        timeout = 10000;
        baseUrl = "http://mail.yandex.ru";
        startMaximized = false;
        browser = selenideBrowser;
        browserPosition = "890x10";
        browserSize = "980x950";
        addListener(new Highlighter());

        open("/lite");
        login();
    }

    @AfterAll
    public static void logout() {
        closeWebDriver();
    }

    private static void login() {
        $("input#passp-field-login").setValue(username).pressEnter();
        $("input#passp-field-passwd").setValue(password).pressEnter();
        //TODO: пропуск привязки телефона (иногда диалог привязки появляется после ввода пароля)
//        SelenideElement el = $("#passp-field-phoneNumber").waitWhile(appears, 5000);
//        if (el.exists()) {
//            $(".button2_theme_normal").click();
//        }
    }
}
